export function notFoundImage(width = 100, height = 100) {
  return `https://dummyimage.com/${width}x${height}/000/ffffff.png&text=Not+Found+:(`;
}

export function isImageExist(imageUrl) {
  return fetch(imageUrl)
    .then((res) => res)
    .catch((err) => err);
}
