import React, { useState } from "react";
import {
  CCard,
  CCardBody,
  CModal,
  CModalHeader,
  CModalBody,
} from "@coreui/react";
import { Form, Input, Button } from "antd";

const Index = (props) => {
  const { onFinish, onFinishFailed } = props;
  const [isShowModal, setIsShowModal] = useState(false);

  return (
    <>
      <CModal
        show={isShowModal}
        onClose={() => setIsShowModal(false)}
        color="success"
        variant="ghost"
        closeOnBackdrop={false}
      >
        <CModalHeader closeButton>Setting Alamat Perangkat</CModalHeader>
        <CModalBody>
          <CCard>
            <CCardBody>
              <Form
                name="basic"
                layout="vertical"
                onFinish={onFinish}
                onFinishFailed={onFinishFailed}
              >
                <Form.Item
                  label="Alamat IP"
                  name="ip-address"
                  rules={[
                    {
                      required: true,
                      message: "Silahkan masukkan alamat ip!",
                    },
                  ]}
                  hasFeedback
                >
                  <Input />
                </Form.Item>

                <Form.Item>
                  <Button type="primary" htmlType="submit" block>
                    Set
                  </Button>
                </Form.Item>
              </Form>
            </CCardBody>
          </CCard>
        </CModalBody>
      </CModal>
      <Button onClick={() => setIsShowModal(true)} type="primary">
        Setting Alamat
      </Button>
    </>
  );
};

export default Index;
