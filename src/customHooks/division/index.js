import { DivisionService } from "../../services";
import useSWR from "swr";
import URL from "../../config/baseUrl";

const useGetDivision = () => {
  return useSWR(URL.DIVISION_TYPE, (URL) =>
    DivisionService.getDivision().then((res) => res)
  );
};

export default { useGetDivision };
