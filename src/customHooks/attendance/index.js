import { AttendanceService } from "../../services";
import useSWR from "swr";
import URL from "../../config/baseUrl";

const useGetAttendance = (keyword, nip, params, key = "") => {
  return useSWR([URL.ATTENDANCE, key], (URL) =>
    AttendanceService.getAttendance(keyword, nip, params).then((res) => res)
  );
};

export default { useGetAttendance };
