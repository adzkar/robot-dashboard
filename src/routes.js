import React from "react";

// const Dashboard = React.lazy(() => import("./views/dashboard/Dashboard"));
const SocialDistancing = React.lazy(() => import("./views/socialDistancing"));
const Thermal = React.lazy(() => import("./views/thermal"));
const FaceRecognation = React.lazy(() => import("./views/faceRecognation"));
const RobotControl = React.lazy(() => import("./views/robotControl"));

const routes = [
  { path: "/", exact: true, name: "Home" },
  { path: "/dashboard", name: "Dashboard", component: SocialDistancing },
  {
    path: "/social-distancing",
    name: "Dashboard",
    component: SocialDistancing,
  },
  { path: "/thermal-imaging", name: "Thermal Imagning", component: Thermal },
  {
    path: "/face-recognation",
    name: "Face Recognation",
    component: FaceRecognation,
  },
  {
    path: "/robot-control",
    name: "Robot Control",
    component: RobotControl,
  },
];

export default routes;
