import React, { lazy } from "react";
import { CCol, CRow } from "@coreui/react";

const SocialDistancing = lazy(() =>
  import("../../containers/socialDistancing")
);

const Index = () => {
  return (
    <CRow>
      <CCol>
        <SocialDistancing />
      </CCol>
    </CRow>
  );
};

export default Index;
