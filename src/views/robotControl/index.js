import React, { lazy } from "react";
import { CCol, CRow } from "@coreui/react";

const RobotControl = lazy(() => import("../../containers/robotControl"));

const Dashboard = () => {
  return (
    <CRow>
      <CCol>
        <RobotControl />
      </CCol>
    </CRow>
  );
};

export default Dashboard;
