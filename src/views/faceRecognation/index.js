import React, { lazy } from "react";
import { CCol, CRow } from "@coreui/react";

const FaceRecognation = lazy(() => import("../../containers/faceRecognation"));

const Dashboard = () => {
  return (
    <CRow>
      <CCol>
        <FaceRecognation />
      </CCol>
    </CRow>
  );
};

export default Dashboard;
