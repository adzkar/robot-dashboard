import BaseService from "../BaseService";
import URL from "../../config/baseUrl";

function getAttendance(keyword, nip, rest) {
  const params = {
    user_name: keyword,
    user_noid: nip,
    ...rest,
  };
  return BaseService.get(URL.ATTENDANCE, { params: params });
}

export default {
  getAttendance,
};
