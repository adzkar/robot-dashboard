export default {
  ATTENDANCE: "/presensi/report",
  DIVISION_TYPE: "/division-type/get",
  USER_BEST: "/dashboard/user/best",
  USER_ABSENCE: "/dashboard/user/bad",
  USER_LATENESS: "dashboard/user/late",
  DASHBOARD_PIE: "/dashboard/pie",
};
