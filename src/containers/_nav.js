export default [
  {
    _tag: "CSidebarNavItem",
    name: "Social Distancing",
    to: "/dashboard",
    icon: "cil-user",
  },
  // {
  //   _tag: "CSidebarNavItem",
  //   name: "Face Recognation",
  //   to: "/face-recognation",
  //   icon: "cil-user",
  // },
  {
    _tag: "CSidebarNavItem",
    name: "Thermal Imaging",
    to: "/thermal-imaging",
    icon: "cil-speedometer",
  },
  {
    _tag: "CSidebarNavItem",
    name: "Kontrol Robot",
    to: "/robot-control",
    icon: "cil-laptop",
  },
];
