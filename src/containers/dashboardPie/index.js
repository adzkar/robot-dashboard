import React from "react";
import { CCard, CCardBody, CCardHeader } from "@coreui/react";
import { CChartPie } from "@coreui/react-chartjs";
import { DashboardCustomHook } from "../../customHooks";

const Index = () => {
  const { data } = DashboardCustomHook.useGetPie();

  return (
    <CCard>
      <CCardHeader>Persentase Kehadiran</CCardHeader>
      <CCardBody>
        {data !== undefined && (
          <CChartPie
            type="pie"
            datasets={[
              {
                backgroundColor: ["#41B883", "#E46651", "#00D8FF", "#DD1B16"],
                data: Object.values(data?.[0]),
              },
            ]}
            labels={Object.keys(data?.[0])}
            options={{
              tooltips: {
                enabled: true,
              },
            }}
          />
        )}
      </CCardBody>
    </CCard>
  );
};

export default Index;
