import React, { useEffect, useState, useRef } from "react";
import { CCard, CCardBody, CCardHeader } from "@coreui/react";
import io from "socket.io-client";
// import { Button } from "antd";

const Index = () => {
  const [image, setImage] = useState();
  const ref = useRef(null);

  useEffect(() => {
    const socket = io.connect("http://192.168.10.34:3000/");
    socket.on("connect", () => {
      console.log("connected");
    });
    socket.on("socialDistancing", (res) => {
      setImage(JSON.parse(res)?.img);
    });
  }, []);

  return (
    <CCard>
      <CCardHeader>
        <div className="container_card_hader">
          <div>Social Distancing</div>
          <div className="components">
            {/* <Button onClick={onAlarm} type="primary">
              Alarm
            </Button> */}
          </div>
        </div>
      </CCardHeader>
      <CCardBody>
        <div className="container_image_content">
          {image && (
            <img
              ref={ref}
              width="85%"
              src={`data:image/jpg;base64, ${image}`}
              alt="video_streaming"
            />
          )}
        </div>
      </CCardBody>
    </CCard>
  );
};

export default Index;
