import React, { useState } from "react";
import {
  CCard,
  CCardBody,
  CCardHeader,
  CDataTable,
  CButton,
  CModal,
  CModalHeader,
  CModalBody,
  CRow,
  CCol,
} from "@coreui/react";
import { faces as data } from "./face.dummy";
import { TimeFormatter } from "../../utils/timeParser";
import "./face.style.scss";

const Index = () => {
  const [isShowModal, setIsShowModal] = useState(false);
  const fields = [
    { key: "no", label: "No" },
    { key: "nip", label: "NIP" },
    { key: "name", label: "Nama" },
    { key: "date", label: "Tanggal" },
    { key: "detail", label: "Detail", _classes: "text-center", sorter: false },
  ];

  return (
    <>
      <CModal
        show={isShowModal}
        onClose={() => setIsShowModal(false)}
        color="success"
        variant="ghost"
      >
        <CModalHeader closeButton>Detail Information</CModalHeader>
        <CModalBody>
          <CRow>
            <CCol md="4">
              <div className="container_face_recognation_image">
                <div
                  style={{
                    backgroundImage:
                      "url('https://images.unsplash.com/photo-1594235623648-adceb49b5336?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=2801&q=80')",
                  }}
                />
              </div>
            </CCol>
            <CCol span="10">
              <CCard>
                <CCardBody>
                  <CRow>
                    <CCol span="2">
                      <strong>Tanggal</strong>
                    </CCol>
                    <CCol span="10">1 September 2020</CCol>
                  </CRow>
                  <CRow>
                    <CCol span="2">
                      <strong>Jam</strong>
                    </CCol>
                    <CCol span="10">10:00</CCol>
                  </CRow>
                  <CRow>
                    <CCol span="2">
                      <strong>NIP</strong>
                    </CCol>
                    <CCol span="10">1232421566</CCol>
                  </CRow>
                  <CRow>
                    <CCol span="2">
                      <strong>Nama</strong>
                    </CCol>
                    <CCol span="10">Willy</CCol>
                  </CRow>
                </CCardBody>
              </CCard>
            </CCol>
          </CRow>
        </CModalBody>
      </CModal>
      <CCard>
        <CCardHeader>Face Recognation</CCardHeader>
        <CCardBody>
          <CDataTable
            items={data}
            fields={fields}
            pagination
            hover
            sorter
            tableFilter
            itemsPerPageSelect
            itemsPerPage={5}
            loading={data === undefined}
            scopedSlots={{
              no: (item, index) => {
                return <td>{index + 1}</td>;
              },
              date: (item) => {
                const parsed = TimeFormatter(item.date);
                return (
                  <td>{`${parsed.date} ${parsed.monthName} ${parsed.year}`}</td>
                );
              },
              detail: (item) => {
                return (
                  <td className="text-center">
                    <CButton
                      color="success"
                      variant="outline"
                      className="px-4"
                      onClick={() => setIsShowModal(true)}
                    >
                      Detail
                    </CButton>
                  </td>
                );
              },
            }}
          />
        </CCardBody>
      </CCard>
    </>
  );
};

export default Index;
