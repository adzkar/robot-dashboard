import React from "react";
import { Slider } from "antd";
import { CContainer } from "@coreui/react";

const Index = () => {
  const marks = {
    0: "Lambat",
    50: "Sedang",
    100: "Cepat",
  };
  return (
    <CContainer>
      <Slider marks={marks} defaultValue={50} step={null} />
    </CContainer>
  );
};

export default Index;
