import React from "react";

const Index = () => {
  return (
    <div className="container_robot_sensor_status">
      {new Array(8).fill(1).map((_, i) => {
        return <div key={i} className="container_robot_dots"></div>;
      })}
    </div>
  );
};

export default Index;
