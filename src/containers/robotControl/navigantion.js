import React, { useState } from "react";
import { CButton, CCard, CCardBody } from "@coreui/react";
import { RightIcon, LeftIcon, TopIcon, ButtomIcon } from "./icons";

const Index = (props) => {
  const { socket } = props;

  const [isClicked, setIsClicked] = useState(false);

  return (
    <CCard className="container_robot_navigation">
      <CCardBody>
        <div className="container_robot_navigation_top">
          <CButton
            color="success"
            className="px-4"
            onClick={() => {
              if (isClicked) {
                socket.emit("control", "stop maju");
              } else {
                socket.emit("control", "maju");
              }
              setIsClicked((curr) => !curr);
            }}
          >
            <TopIcon />
            Maju
          </CButton>
        </div>
        <div className="container_robot_navigation_middle">
          <CButton
            color="success"
            className="px-4"
            onClick={() => {
              if (isClicked) {
                socket.emit("control", "stop kiri");
              } else {
                socket.emit("control", "kiri");
              }
              setIsClicked((curr) => !curr);
            }}
          >
            <LeftIcon />
            Kiri
          </CButton>
          <CButton
            color="success"
            className="px-4"
            onClick={() => {
              if (isClicked) {
                socket.emit("control", "stop kanan");
              } else {
                socket.emit("control", "kanan");
              }
              setIsClicked((curr) => !curr);
            }}
          >
            <RightIcon />
            Kanan
          </CButton>
        </div>
        <div className="container_robot_navigation_top">
          <CButton
            color="success"
            className="px-4"
            onClick={() => {
              if (isClicked) {
                socket.emit("control", "stop mundur");
              } else {
                socket.emit("control", "mundur");
              }
              setIsClicked((curr) => !curr);
            }}
          >
            <ButtomIcon />
            Mundur
          </CButton>
        </div>
      </CCardBody>
    </CCard>
  );
};

export default Index;
