import React, { useEffect } from "react";
import {
  CCard,
  CCardBody,
  // CCardTitle,
  // CCardHeader,
  CRow,
  // CCol,
  // CWidgetProgressIcon,
} from "@coreui/react";
import io from "socket.io-client";
// import { BatteryIcon } from "./icons";
import "./style.scss";

// import Navigation from "./navigantion";
// import Pole from "./pole";
// import Emergency from "./emergency";
// import SensorStatus from "./sensorStatus";
// import LEDStatus from "./ledStatus";
// import SetSpeed from "./setSpeed";
// import MultipurposeButton from "./multipurposeButton";

const Index = () => {
  const socket = io.connect("localhost:5000");

  useEffect(() => {
    socket
      .on("connect", function () {
        socket.emit("join_group", { channel: "robot" });
        console.log("connected");
      })
      .on("data_info_robot", (res) => {
        console.log(res);
      });
  });

  return (
    <>
      <CRow>
        <h1>Kontrol Robot</h1>
      </CRow>
      <CRow>
        <CCard>
          <CCardBody>
            <CRow>
              <a
                href="http://192.168.10.111:5000"
                target="_blank"
                className="button_link"
                rel="noopener noreferrer"
              >
                Klik Disini
              </a>
            </CRow>
            {/* <CButton color="success" variant="outline" className="button_link"> */}
            {/* </CButton> */}
          </CCardBody>
        </CCard>
        {/* <CCol md="10">
          <CCard>
            <CCardHeader>Navigasi Robot</CCardHeader>
            <CCardBody>
              <CRow>
                <CCol>
                  <Navigation socket={socket} />
                </CCol>
                <CCol>
                  <Pole socket={socket} />
                </CCol>
              </CRow>
            </CCardBody>
          </CCard>
        </CCol>
        <CCol md="2" sm="4">
          <CWidgetProgressIcon
            header="28%"
            text="Kondisi Baterai"
            color="gradient-success"
          >
            <BatteryIcon />
          </CWidgetProgressIcon>
          <MultipurposeButton />
        </CCol>
      </CRow>
      <CRow>
        <CCol>
          <CCard>
            <CCardHeader>Emergency</CCardHeader>
            <CCardBody>
              <Emergency />
            </CCardBody>
          </CCard>
        </CCol>
        <CCol md="8" sm="4">
          <CCard>
            <CCardHeader>Status Sensor</CCardHeader>
            <CCardBody>
              <SensorStatus />
            </CCardBody>
          </CCard>
        </CCol>
      </CRow>
      <CRow>
        <CCol>
          <CCard>
            <CCardHeader>Status LED</CCardHeader>
            <CCardBody>
              <LEDStatus />
            </CCardBody>
          </CCard>
        </CCol>
        <CCol>
          <CCard>
            <CCardHeader>Atur Kecepatan</CCardHeader>
            <CCardBody>
              <SetSpeed />
            </CCardBody>
          </CCard>
        </CCol> */}
      </CRow>
    </>
  );
};

export default Index;
