import React from "react";
import { CButton } from "@coreui/react";
import { ToolOutlined } from "@ant-design/icons";

const Index = () => {
  return (
    <div className="container_robot_navigation">
      <CButton color="success">
        <ToolOutlined
          style={{
            fontSize: "20px",
          }}
        />
        Multipurpose Button
      </CButton>
    </div>
  );
};

export default Index;
