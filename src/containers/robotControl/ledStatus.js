import React from "react";

const Index = () => {
  return (
    <div className="container_robot_sensor_status">
      <div className="container_robot_dots"></div>
      <div className="container_robot_dots bg-warning"></div>
      <div className="container_robot_dots bg-success"></div>
    </div>
  );
};

export default Index;
