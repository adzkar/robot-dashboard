import React from "react";
import { CButton } from "@coreui/react";
import { CloseCircleOutlined } from "@ant-design/icons";

const Index = () => {
  return (
    <div className="container_robot_navigation">
      <CButton color="danger">
        <CloseCircleOutlined
          style={{
            fontSize: "20px",
          }}
        />
        Emergency
      </CButton>
    </div>
  );
};

export default Index;
