import React from "react";

export const BatteryIcon = () => {
  return (
    <svg
      xmlns="http://www.w3.org/2000/svg"
      viewBox="0 0 512 512"
      className="c-icon c-icon-2xl"
      role="img"
    >
      <rect
        width="32"
        height="200"
        x="80"
        y="160"
        fill="var(--ci-primary-color, currentColor)"
        className="ci-primary"
      ></rect>
      <rect
        width="32"
        height="200"
        x="144"
        y="160"
        fill="var(--ci-primary-color, currentColor)"
        className="ci-primary"
      ></rect>
      <rect
        width="32"
        height="200"
        x="208"
        y="160"
        fill="var(--ci-primary-color, currentColor)"
        className="ci-primary"
      ></rect>
      <rect
        width="32"
        height="200"
        x="272"
        y="160"
        fill="var(--ci-primary-color, currentColor)"
        className="ci-primary"
      ></rect>
      <rect
        width="32"
        height="200"
        x="336"
        y="160"
        fill="var(--ci-primary-color, currentColor)"
        className="ci-primary"
      ></rect>
      <path
        fill="var(--ci-primary-color, currentColor)"
        d="M432,176V119.59A23.825,23.825,0,0,0,408,96H39.986a23.825,23.825,0,0,0-24,23.59V400.41a23.825,23.825,0,0,0,24,23.59H408a23.825,23.825,0,0,0,24-23.59V344h64V176Zm32,136H400v80H47.986V128H400v80h64Z"
        className="ci-primary"
      ></path>
    </svg>
  );
};

export const ButtomIcon = () => {
  return (
    <svg
      xmlns="http://www.w3.org/2000/svg"
      viewBox="0 0 512 512"
      className="c-icon c-icon-2xl"
      role="img"
    >
      <path
        fill="var(--ci-primary-color, currentColor)"
        d="M255.682,494.636,16,254.3V216.024l143.937-.007V16h192V216.007L495.952,216l-.035,38.688ZM54.931,248.022l200.8,201.342L457.328,248l-137.391.008V48h-128V248.015Z"
        className="ci-primary"
      ></path>
    </svg>
  );
};

export const LeftIcon = () => {
  return (
    <svg
      xmlns="http://www.w3.org/2000/svg"
      viewBox="0 0 512 512"
      className="c-icon c-icon-2xl"
      role="img"
    >
      <path
        fill="var(--ci-primary-color, currentColor)"
        d="M294.637,496l-38.688-.035L16,255.729,256.334,16.048h38.277l.008,143.937H494.636v192H294.629ZM61.271,255.773l201.364,201.6-.008-137.391H462.636v-128H262.621l-.008-137.006Z"
        className="ci-primary"
      ></path>
    </svg>
  );
};

export const RightIcon = () => {
  return (
    <svg
      xmlns="http://www.w3.org/2000/svg"
      viewBox="0 0 512 512"
      className="c-icon c-icon-2xl"
      role="img"
    >
      <path
        fill="var(--ci-primary-color, currentColor)"
        d="M254.3,496H216.025l-.008-143.937H16v-192H216.007L216,16.048l38.688.035L494.636,256.318ZM48,320.063H248.015l.007,137.006,201.342-200.8L248,54.672l.008,137.391H48Z"
        className="ci-primary"
      ></path>
    </svg>
  );
};

export const TopIcon = () => {
  return (
    <svg
      xmlns="http://www.w3.org/2000/svg"
      viewBox="0 0 512 512"
      className="c-icon c-icon-2xl"
      role="img"
    >
      <path
        fill="var(--ci-primary-color, currentColor)"
        d="M352.063,496h-192V295.993L16.047,296l.037-38.688L256.318,17.364,496,257.7v38.278l-143.937.006Zm-160-32h128V263.984l137.006-.006L256.274,62.636,54.672,264l137.391-.008Z"
        className="ci-primary"
      ></path>
    </svg>
  );
};
