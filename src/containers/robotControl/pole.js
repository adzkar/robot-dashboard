import React, { useState } from "react";
import { CButton, CCard, CCardTitle, CCardBody } from "@coreui/react";
import { TopIcon, ButtomIcon } from "./icons";

const Index = (props) => {
  const { socket } = props;

  const [isClicked, setIsClicked] = useState(false);

  return (
    <CCard className="container_robot_navigation">
      <CCardBody>
        <CCardTitle>Kontrol Tiang</CCardTitle>
        <div className="container_robot_navigation_top">
          <CButton
            color="success"
            className="px-4"
            onClick={() => {
              if (isClicked) {
                socket.emit("control", "stop naik");
              } else {
                socket.emit("control", "naik");
              }
              setIsClicked((curr) => !curr);
            }}
          >
            <TopIcon />
            Atas
          </CButton>
        </div>
        <div className="container_robot_navigation_top">
          <CButton
            color="success"
            className="px-4"
            onClick={() => {
              if (isClicked) {
                socket.emit("control", "stop turun");
              } else {
                socket.emit("control", "turun");
              }
              setIsClicked((curr) => !curr);
            }}
          >
            <ButtomIcon />
            Bawah
          </CButton>
        </div>
      </CCardBody>
    </CCard>
  );
};

export default Index;
